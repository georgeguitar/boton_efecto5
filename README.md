# Proyecto Final del Módulo de Gestión de Paquetes

## Autor

Navarro Arias Juan Dirceu

## Resumen

Paquete que se puede utilizar para añadir un efecto de desvanecimiento y hacer aparecer un carrito de compras a un botón html mediante CSS.
La versión "beta" tiene la base y sobre esa se construyó la versión "release".

Se utilizó el gestor de paquetes NPM, el nombre del paquete es: "boton_efecto"

Disponible en:
 
* Paquete de [npmjs](https://www.npmjs.com/package/boton_efecto5/ "boton_efecto")
* Repositorio en [gitlab](https://gitlab.com/georgeguitar/boton_efecto5.git/ "boton_efecto")

## Modo de uso

Para utilizar el paquete, se debe instalar con:

Versión beta:
``` $ npm install boton_efecto5@1.0.0-beta.0```

Versión release:
``` $ npm install boton_efecto5@1.1.1```


Una vez instalado el paquete, se debe incluir el archivo "button.css"  en el código html.
```
<head>
<link href="node_modules/boton_efecto5/button.css" type="text/css" rel="stylesheet" />
</head>
```
El paquete descargado tienen dos archivos html para hacer la prueba inmediata de la funcionalidad:

* ``` ejemplo-efecto_carrito_de_compras.html ```
* ``` ejemplo-efecto_desvanecimiento.html ```

## Botón con efecto de desvanecimiento.

![ejemplo](screenshot.png)

Para utilizar el efecto, se tiene que incluir el estilo "button".

```
<button type="button" onclick="alert('&iexcl;Opci&oacute;n 1!')" class="button">&iexcl;Opci&oacute;n 1!</button>
```

Ejemplo:
```
<!DOCTYPE html>
<html>

<head>
	<link href="node_modules/boton_efecto5/button.css" type="text/css" rel="stylesheet" />
</head>

<body>
	<h1>Bot&oacute;n con efectos</h1>
	<p>Autor: Juan Navarro</p>

	<button type="button" onclick="alert('&iexcl;Opci&oacute;n 1!')" class="button">&iexcl;Opci&oacute;n 1!</button>

</body>
</html>
```
Preview:
![ejemplo](screenshot1.png)

## Botón con carrito de compra.

![ejemplo](screenshot2.png)

Se tienen un efecto de carrito de compra en el botón.
Para utilizar el efecto, se tiene que incluir el estilo "button1".

```
<button type="button" onclick="alert('&iexcl;Opci&oacute;n 2!')" class="button1">&iexcl;Opci&oacute;n 2!</button>
```

Ejemplo:
```
<!DOCTYPE html>
<html>

<head>
	<link href="node_modules/boton_efecto5/button.css" type="text/css" rel="stylesheet" />
</head>

<body>
	<h1>Bot&oacute;n con efectos</h1>
	<p>Autor: Juan Navarro</p>

	<button type="button" onclick="alert('&iexcl;Opci&oacute;n 2!')" class="button1">&iexcl;Opci&oacute;n 2!</button>

</body>
</html>
```


Preview:
![ejemplo](screenshot3.png)

